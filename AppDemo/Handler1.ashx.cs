﻿using System.Data;
using System.Data.SqlClient;
using System;
using System.Web;
public class Handler : IHttpHandler
{
    public void ProcessRequest(HttpContext context)
    {
        string doType = context.Request.QueryString["DoType"];

        if (doType == "StudentList_Delete")
        {
            string no = context.Request["No"];
            string sql = "DELETE FROM Demo_Student WHERE No='"+no+"'";
            this.RunSQL(sql);
            context.Response.ContentType = "text/plain";
            context.Response.Write("删除成功...");
            return;
        }

        if (doType == "Login_Submit")
        {
            string info = Login_Submit(context);
            context.Response.ContentType = "text/plain";
            context.Response.Write(info);
            return;
        }

        //增加学生.
        if (doType == "AddStudent_Save")
        {
            string info = AddStudent_Save(context);
            context.Response.ContentType = "text/plain";
            context.Response.Write(info);
            return;
        }

        //学生列表.
        if (doType == "StudentList_Init")
        {
            string str = StudentList_Init();

            context.Response.ContentType = "text/plain";
            context.Response.Write(str);
            return;
        }
    }
    public string AddStudent_Save(HttpContext context)
    {
        try
        {
            string no = context.Request.QueryString["No"];
            string name = context.Request.QueryString["Name"];
            string tel = context.Request.QueryString["Tel"];

            //检查编号是否重复.
            string sql = "SELECT No FROM Demo_Student WHERE No='" + no + "'";
            System.Data.DataTable dt = this.RunSQLReturnTable(sql);
            if (dt.Rows.Count!= 0)
            {
                return "err@学号["+no+"]重复.";
            }

            //插入到数据库.
            sql = "INSERT INTO Demo_Student (No,Name,Tel) values('"+no+"','"+name+"','"+tel+"')";
            this.RunSQL(sql);
            return "增加成功";
        }
        catch (Exception ex)
        {
            return "err@增加失败:"+ex.Message;
        }
    }
    /// <summary>
    /// 返回学生列表信息.
    /// </summary>
    /// <param name="context"></param>
    public string StudentList_Init()
    {
        string sql = "SELECT *  FROM Demo_Student";
        //执行查询，返回查询结果.
        System.Data.DataTable dt = this.RunSQLReturnTable(sql);
        string str = Newtonsoft.Json.JsonConvert.SerializeObject(dt);
        return str;
    }
    public string Login_Submit(HttpContext context)
    {
        try
        {
            string userNo = context.Request.QueryString["UserNo"];
            string pass = context.Request.QueryString["Pass"];
            string sql = "SELECT No,Name,Pass FROM Port_Emp where No='" + userNo + "'";

            //执行查询，返回查询结果.
            System.Data.DataTable dt = this.RunSQLReturnTable(sql);

            if (dt.Rows.Count == 0)
            {
                return "err@用户名错误.";
            }

            if (pass != dt.Rows[0]["Pass"].ToString())
            {
                return "err@密码错误.";
            }
            return "登陆成功";
        }
        catch (Exception ex)
        {
            return "err@" + ex.Message;
        }
    }

    public int RunSQL(string sql)
    {
        string dsn = System.Configuration.ConfigurationManager.AppSettings["DBConnStr"];

        SqlConnection conn = new SqlConnection(dsn);
        if (conn.State != ConnectionState.Open)
            conn.Open();

        if (conn == null)
            conn = new SqlConnection(dsn);

        if (conn.State != System.Data.ConnectionState.Open)
        {
            conn.ConnectionString = dsn;
            conn.Open();
        }

        SqlCommand cmd = new SqlCommand(sql, conn);
        cmd.CommandType = CommandType.Text;
        int i = 0;
        try
        {
            i = cmd.ExecuteNonQuery();
        }
        catch  
        {
            cmd.Dispose();
        }
        cmd.Dispose();
        conn.Close();

        return i;
    }
    /// <summary>
    /// 运行SQL返回datatble
    /// </summary>
    /// <param name="sql"></param>
    /// <returns></returns>
    public System.Data.DataTable RunSQLReturnTable(string sql)
    {
        string dsn = System.Configuration.ConfigurationManager.AppSettings["DBConnStr"];

        SqlConnection conn = new SqlConnection(dsn);
        if (conn.State != ConnectionState.Open)
            conn.Open();

        SqlDataAdapter ada = new SqlDataAdapter(sql, conn);
        ada.SelectCommand.CommandType = CommandType.Text;

        try
        {
            DataTable oratb = new DataTable("otb");
            ada.Fill(oratb);
            ada.Dispose();
            conn.Close();
            return oratb;
        }
        catch (Exception ex)
        {
            ada.Dispose();
            conn.Close();
            throw new Exception("SQL=" + sql + " Exception=" + ex.Message);
        }
    }

    public bool IsReusable
    {
        get
        {
            return false;
        }
    }

}
