
-- 常见查询统计sql 语句.

-- 查询全部.
SELECT * FROM Port_Emp 

-- 查询前10条.
SELECT top 10 * FROM Port_Emp 

-- 按照编号升序,排列.
SELECT * FROM Port_Dept ORDER BY No

--按照降序排列.
SELECT * FROM Port_Dept ORDER BY No DESC

--按照指定的列查询. 
SELECT No,Name FROM Port_Dept

-- 高级单表查询
SELECT TOP 10 No,Name FROM Port_Emp ORDER BY No DESC


-- 查询条件.
SELECT * FROM Port_Emp WHERE No ='zhoutianjiao'

SELECT * FROM Port_Emp WHERE No ='zhoutianjiao' and Name='周天娇'

SELECT * FROM Port_Emp WHERE No ='zhoutianjiao' or No='liyan' 

-- 去掉重复的名字查询.
SELECT DISTINCT No, Name FROM Port_Emp

-- 统计分析, 求平均年龄. 
SELECT AVG(Age) FROM Demo_Student

-- 求男生平均年龄.
SELECT AVG(Age) FROM Demo_Student WHERE XB=1


-- 求最大年龄. 
SELECT MAX(Age) FROM Demo_Student

-- 求男生最大年龄.
SELECT MAX(Age) FROM Demo_Student WHERE XB=1

-- 求最小年龄. 
SELECT Min(Age) FROM Demo_Student

-- 求男生最小年龄.
SELECT Min(Age) FROM Demo_Student WHERE XB=1


/**    
 关联查询, 把两个表关联在一起. 
 **/

SELECT Port_Emp.No, Port_Emp.Name, Port_Dept.Name AS DeptName FROM Port_Emp , Port_Dept WHERE Port_Emp.FK_Dept=Port_Dept.No

SELECT A.No, A.Name, B.Name AS DeptName FROM Port_Emp A, Port_Dept B WHERE A.FK_Dept=B.No